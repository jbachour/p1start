package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList <E> implements SortedList<E> {
	/*
	 * node class starts
	 * */
	private static class Node<E> {

		private E element;
		private Node<E> next,prev;

		public Node(E element, Node<E> next, Node <E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		public Node() {
			super();
		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}

	/*
	 * Node class ends
	 * */
	private Node<E> header;
	private int currentSize;
	private Comparator<E> comparator;
	/*
	 * Constructor
	 * */
	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.header = new Node<>();
		this.currentSize=0;
		this.comparator=comparator;
	}

	/*
	 * Iterator
	 * */
	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	private class CircularSortedDoublyLinkedListIterator <E> implements Iterator<E>{
		private Node<E> nextNode;

		CircularSortedDoublyLinkedListIterator() {
			this.nextNode=(Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return this.nextNode.getElement()!=null;
		}
		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode=this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}

	public boolean add(E obj) {
		if(obj == null) return false;
		if(this.isEmpty()) {

			Node<E> newNode = new Node<E>();
			newNode.setElement(obj);
			newNode.setNext(this.header);
			newNode.setPrev(this.header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			this.currentSize++;
			return true;
		}
		else {
			Node<E> newNode = new Node<E>();
			newNode.setElement(obj);

			Node<E> target = this.header.getNext();
			while(target!=this.header && this.comparator.compare(target.getElement(), newNode.getElement()) < 0) {
				target = target.getNext();
			}
			newNode .setNext(target);
			newNode .setPrev(target.getPrev());
			target.getPrev().setNext(newNode);
			target.setPrev(newNode);


			this.currentSize++;
			return true;
		}
	}

	public int size() {
		return this.currentSize;
	}
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if (i < 0) {
			return false;
		}else {
			this.remove(i);
			return true;
		}
	}
	public boolean remove(int index) {
		if(index <0 || index > this.currentSize ) throw new IndexOutOfBoundsException();

		Node <E> target = this.header.getNext();
		int i=0;

		while(index != i) {
			target = target.getNext();
			i++;
		}
		target.getPrev().setNext(target.getNext());
		target.getNext().setPrev(target.getPrev());

		target.setNext(null);
		target.setPrev(null);
		target.setElement(null);
		this.currentSize--;
		return true;

	}
	public int removeAll(E obj) {
		int counter=0;
		while(this.contains(obj)) {
			remove(obj);
			counter++;
		}
		return counter;
	}
	public E first() {
		return (this.isEmpty() ? null : this.header.getNext().getElement());
	}
	public E last() {
		return (this.isEmpty() ? null : this.header.getPrev().getElement());
	}

	public E get(int index) {
		if(index <0 || index >= this.currentSize ) throw new IndexOutOfBoundsException();
		else if(this.isEmpty()) {return null;}
		Node <E> target = this.header.getNext();
		int i=0;
		while(index != i) {
			target = target.getNext();
			i++;
		}
		return target.getElement();

	}
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}
	public boolean contains(E e) {
		return firstIndex(e)!=-1;

	}
	public boolean isEmpty() {
		return this.currentSize==0;
	}
	public int firstIndex(E e) {
		for(int i=0; i<this.size();++i) {
			if(this.get(i).equals(e)) {
				return i;
			}
		}
		return -1;
	}
	public int lastIndex(E e) {
		for(int i=this.size()-1; i>=0;--i) {
			if(this.get(i).equals(e)) {
				return i;
			}
		}
		return -1;
	}

}