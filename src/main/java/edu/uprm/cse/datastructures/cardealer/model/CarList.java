package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {

	private static CarList singleton = new CarList();
	private static CircularSortedDoublyLinkedList<Car> carList;

	private CarList() {
		carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	}
    public CircularSortedDoublyLinkedList<Car> getCarList(){
		return carList;
	}
	public static void resetCars(){
		carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	}

	public static CarList getInstance(){
		return singleton;
	}
}