package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.Path;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance().getCarList();

	//a.read all cars as an array of cars /cars
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] cars=new Car[carList.size()];

		for(int i=0; i<cars.length;i++){
			cars[i]=carList.get(i);
		}
		return cars;
	}
	/*
	 * b.read a car with a given id cars/{id}
	 * */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {

		for (int i = 0; i < carList.size(); i++) {
			if (carList.get(i).getCarId() == id) {
				return carList.get(i);
			}
		}
		throw new NotFoundException();
	}
	/*
	 * c.add a new car to the system /add
	 *  */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car newCar) {
		for(int i = 0; i< carList.size(); i++){
			if(carList.get(i).getCarId() == newCar.getCarId()){
				return Response.status(Response.Status.FORBIDDEN).build();
			}
		}
		carList.add(newCar);
		return Response.status(201).build();
	}
	/*
	 * d.update existing car /cars/{id}/update
	 * 
	 * */
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for (int i = 0; i < carList.size(); i++) {
			if (carList.get(i).getCarId() == car.getCarId()) {
				carList.remove(i); 
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build(); 
	}
	/*
	 * e.delete car cars/{id}/delete
	 * 
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		for (int i = 0; i < carList.size(); i++) {
			if (carList.get(i).getCarId() == id) {
				carList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
